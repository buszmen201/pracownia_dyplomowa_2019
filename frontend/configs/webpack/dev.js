const merge = require('webpack-merge');
const webpack = require('webpack');
const commonCfg = require('./common');

module.exports = merge(commonCfg, {
    mode: 'development',
    entry: [
        'react-hot-loader/patch', // activate HMR for React
        'webpack-dev-server/client?http://localhost:8080',
        'webpack/hot/only-dev-server',
        './index.tsx'
    ],
    devServer: {
        historyApiFallback: true,
        hot: true,
    },
    devtool: 'cheap-module-eval-source-map',
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
    ],
});
