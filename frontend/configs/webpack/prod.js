const merge = require('webpack-merge');
const { resolve } = require('path');

const commonCfg = require('./common');

module.exports = merge(commonCfg, {
    mode: 'production',
    entry: './index.tsx',
    output: {
        filename: 'js/bundle.[hash].min.js',
        path: resolve(__dirname, '../../dist'),
        publicPath: '/',
    },
    devtool: 'source-map',
    plugins: [],
});
