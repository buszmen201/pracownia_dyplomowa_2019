const tsc = require('typescript');
const tsconfig = require('./../tsconfig.json');

module.exports = {
    process(src, path) {
        const isTS = path.endsWith('.ts');
        const isTSX = path.endsWith('.tsx');
        const isTypescript = (isTS || isTSX);

        if (isTypescript) {
            return tsc.transpileModule(
                src,
                {
                    compilerOptions: tsconfig.compilerOptions,
                    fileName: path,
                }
            ).outputText;
        }
        return src;
    },
};
