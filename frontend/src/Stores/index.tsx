import {default as React, useContext} from 'react';
import { useLocalStore } from 'mobx-react-lite';
import {createThemeStore, TThemeStore} from '~/Stores/Theme.store';

interface IStore {
  Theme: TThemeStore;
}

interface IStoreProps {
  children:   JSX.Element;
}

const StoreContext = React.createContext<IStore | null>(null);


const StoresProvider = ({ children }: IStoreProps) => {
  const Provider = StoreContext.Provider;

  const combinedStores: IStore = {
    Theme: useLocalStore(createThemeStore),
  };

  return (
    <Provider value={combinedStores}>
      {children}
    </Provider>
  )

};

const useStores = () => {
  const stores = useContext(StoreContext);
  (window as any).store = stores;
  const up = new Error('You have forgot to use StoreProvider, shame on you.');
  if (!stores) {
    throw up;
  }
  return stores;
};

export {
  StoresProvider,
  useStores,
}
