import {Collapse, List, ListItem, ListItemIcon, ListItemText, SvgIcon, Tooltip} from "@material-ui/core";
import { Link } from 'react-router-dom';
import {
  ExpandLess,
  ExpandMore,
  Category as CategoryIcon,
  Star as StarIcon,
} from "@material-ui/icons";
import {default as React, useState} from "react";
import {useStyles} from "~/Assets/jss/main.jss";
import {useStores} from "~/Stores";
import {SvgIconProps} from "@material-ui/core/SvgIcon";

const TrendingIcon = (props: SvgIconProps) => {
  return (
      <SvgIcon {...props}>
        <path d="M16 6l2.29 2.29-4.88 4.88-4-4L2 16.59 3.41 18l6-6 4 4 6.3-6.29L22 12V6z"/>
      </SvgIcon>
  );
};

const NewReleasesIcon = (props: SvgIconProps) => {
  return (
      <SvgIcon {...props}>
        <path d="M23 12l-2.44-2.78.34-3.68-3.61-.82-1.89-3.18L12 3 8.6 1.54 6.71 4.72l-3.61.81.34 3.68L1 12l2.44 2.78-.34 3.69 3.61.82 1.89 3.18L12 21l3.4 1.46 1.89-3.18 3.61-.82-.34-3.68L23 12zm-10 5h-2v-2h2v2zm0-4h-2V7h2v6z"/>
      </SvgIcon>
  );
};


const UpcomingIcon = (props: SvgIconProps) => {
  return (
      <SvgIcon {...props}>
        <path d="M2.5 19h19v2h-19zm7.18-5.73l4.35 1.16 5.31 1.42c.8.21 1.62-.26 1.84-1.06.21-.8-.26-1.62-1.06-1.84l-5.31-1.42-2.76-9.02L10.12 2v8.28L5.15 8.95l-.93-2.32-1.45-.39v5.17l1.6.43 5.31 1.43z"/>
      </SvgIcon>
  );
};

const HorrorCategoryIcon = (props: SvgIconProps) => {
  return (
      <SvgIcon {...props} >
        <path d="M9.043,13.852l2.582,1.728c-0.622,4.832-6.143,8.545-6.143,8.545l-2.034-1.898L9.043,13.852z
		 M20.677,0l-0.039,0.584c-0.674,10.17-5.928,15.016-5.982,15.065l-0.143,0.129l-4.956-3.31l6.442-8.994
		c0.361-0.566,1.615-2.461,2.299-2.597c0.18-0.035,0.343,0.028,0.445,0.174c0.017,0.021,0.059,0.082,0.25,0.044
		c0.418-0.083,1.032-0.545,1.234-0.716L20.677,0z M19.887,1.234c-0.252,0.154-0.538,0.298-0.798,0.349
		c-0.402,0.081-0.619-0.081-0.723-0.209c-0.355,0.13-1.277,1.315-1.955,2.379l-6.153,8.587l4.202,2.806
		c0.488-0.494,2.19-2.374,3.617-5.707L18.05,9.398c0,0,0.938-3.564-0.174-3.85c-1.112-0.285-0.955-3.313,0.328-2.598
		C19.203,3.508,19.708,1.952,19.887,1.234z M8.595,13.225l4.081,2.724l0.514-0.768l-4.082-2.725L8.595,13.225z M18.66,10.554
		c0,0-0.471,1.026-0.375,1.507c0.096,0.479,0.465,0.813,0.822,0.741c0.359-0.072,0.572-0.52,0.477-0.999
		C19.489,11.322,18.66,10.554,18.66,10.554z M19.288,9.152c0,0-0.16,0.353-0.129,0.517c0.033,0.165,0.158,0.278,0.282,0.254
		c0.123-0.024,0.196-0.177,0.164-0.342S19.288,9.152,19.288,9.152z"/>
      </SvgIcon>
  );
};




const MenuList = () => {
  const classes = useStyles(useStores().Theme.internal.theme);
  const [list1State, setList1State] = useState(false);
  const handleList1Click = () => setList1State(!list1State);

  return (
      <>
        <Tooltip title={"Trending"} placement={"right"}>
          <Link to={"/trending"}>
            <ListItem button={true}>
              <ListItemIcon>
                <TrendingIcon />
              </ListItemIcon>
              <ListItemText primary="Trending"/>
            </ListItem>
          </Link>
        </Tooltip>
        <Tooltip title={"New Releases"} placement={"right"}>
          <ListItem button>
            <ListItemIcon>
              <NewReleasesIcon />
            </ListItemIcon>
            <ListItemText primary="New releases"/>
          </ListItem>
        </Tooltip>
        <Tooltip title={"Upcoming"} placement={"right"}>
          <ListItem button>
            <ListItemIcon>
              <UpcomingIcon />
            </ListItemIcon>
            <ListItemText primary="Upcoming"/>
          </ListItem>
        </Tooltip>
        <Tooltip title={"Categories"} placement={"right"}>
          <ListItem button onClick={handleList1Click}>
            <ListItemIcon>
              <CategoryIcon />
            </ListItemIcon>
            <ListItemText primary="Categories"/>
            {list1State ? <ExpandLess/> : <ExpandMore/>}
          </ListItem>
        </Tooltip>
        <Collapse in={list1State} timeout="auto" unmountOnExit={true}>
          <List component="div" disablePadding={true}>
            <Tooltip title={"Horror"} placement={"right"}>
              <ListItem button className={classes.nested}>
                <ListItemIcon>
                  <HorrorCategoryIcon/>
                </ListItemIcon>
                <ListItemText primary="Horror"/>
              </ListItem>
            </Tooltip>
            <Tooltip title={"Fairytale"} placement={"right"}>
              <ListItem button className={classes.nested}>
                <ListItemIcon>
                  <StarIcon/>
                </ListItemIcon>
                <ListItemText primary="Fairytale"/>
              </ListItem>
            </Tooltip>
          </List>
        </Collapse>
      </>
  )
};

export {
  MenuList
}
