import {default as React, useCallback, useEffect, useRef, useState} from 'react';
import { IRoute } from "~Routes";

import {
  CssBaseline,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Drawer,
  Divider,
  List,
  Container,
  Link,
  Tooltip,
  SvgIcon,
  Collapse,
  Paper,
  InputBase,
  MuiThemeProvider,
} from '@material-ui/core';

import {
  Menu as MenuIcon,
  Notifications as NotificationIcon,
  ChevronLeft as ChevronLeftIcon,
  KeyboardArrowDown as KeyboardArrowDownIcon,
  Book as BookIcon,
  Search as SearchIcon,
} from '@material-ui/icons'

import {useStyles} from "~/Assets/jss/main.jss";
import { Route } from 'react-router-dom';
import { default as clsx } from 'clsx';
import {SvgIconProps} from "@material-ui/core/SvgIcon";
import {useStores} from "~/Stores";
import {observer} from "mobx-react-lite";
import {SearchResult} from "~Components/common/SearchResult";
import {MenuList} from "~Components/common/Menu";
import useEventListener from "@use-it/event-listener";

interface IDefaultLayout extends IRoute {
}

const LightBulbIcon = (props: SvgIconProps) => {
  return (
      <SvgIcon {...props}>
        <path
            d="M9 21c0 .55.45 1 1 1h4c.55 0 1-.45 1-1v-1H9v1zm3-19C8.14 2 5 5.14 5 9c0 2.38 1.19 4.47 3 5.74V17c0 .55.45 1 1 1h6c.55 0 1-.45 1-1v-2.26c1.81-1.27 3-3.36 3-5.74 0-3.86-3.14-7-7-7zm2.85 11.1l-.85.6V16h-4v-2.3l-.85-.6C7.8 12.16 7 10.63 7 9c0-2.76 2.24-5 5-5s5 2.24 5 5c0 1.63-.8 3.16-2.15 4.1z"
        />
      </SvgIcon>
  );
};

const dummyList = [
  {
    callback: () => console.log('fired'),
    image: 'https://via.placeholder.com/150x200.jpg',
    title: 'Cujo',
    author: 'Stephen King',
    desc: 'This impressive paella is a perfect party dish and a fun meal to cook together with your  guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.',
  },
  {
    callback: () => console.log('fired'),
    image: 'https://via.placeholder.com/150x200.jpg',
    title: 'Cujo',
    author: 'Stephen King',
    desc: 'This impressive paella is a perfect party dish and a fun meal to cook together with your  guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.',
  },
  {
    callback: () => console.log('fired'),
    image: 'https://via.placeholder.com/150x200.jpg',
    title: 'Cujo',
    author: 'Stephen King',
    desc: 'This impressive paella is a perfect party dish and a fun meal to cook together with your  guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.',
  },
  {
    callback: () => console.log('fired'),
    image: 'https://via.placeholder.com/150x200.jpg',
    title: 'Cujo',
    author: 'Stephen King',
    desc: 'This impressive paella is a perfect party dish and a fun meal to cook together with your  guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.',
  },
  {
    callback: () => console.log('fired'),
    image: 'https://via.placeholder.com/150x200.jpg',
    title: 'Cujo',
    author: 'Stephen King',
    desc: 'This impressive paella is a perfect party dish and a fun meal to cook together with your  guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.',
  },
  {
    callback: () => console.log('fired'),
    image: 'https://via.placeholder.com/150x200.jpg',
    title: 'Cujo',
    author: 'Stephen King',
    desc: 'This impressive paella is a perfect party dish and a fun meal to cook together with your  guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.',
  },
  {
    callback: () => console.log('fired'),
    image: 'https://via.placeholder.com/150x200.jpg',
    title: 'Cujo',
    author: 'Stephen King',
    desc: 'This impressive paella is a perfect party dish and a fun meal to cook together with your  guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.',
  },
  {
    callback: () => console.log('fired'),
    image: 'https://via.placeholder.com/150x200.jpg',
    title: 'Cujo',
    author: 'Stephen King',
    desc: 'This impressive paella is a perfect party dish and a fun meal to cook together with your  guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.',
  },
  {
    callback: () => console.log('fired'),
    image: 'https://via.placeholder.com/150x200.jpg',
    title: 'Cujo',
    author: 'Stephen King',
    desc: 'This impressive paella is a perfect party dish and a fun meal to cook together with your  guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.',
  },
  {
    callback: () => console.log('fired'),
    image: 'https://via.placeholder.com/150x200.jpg',
    title: 'Cujo',
    author: 'Stephen King',
    desc: 'This impressive paella is a perfect party dish and a fun meal to cook together with your  guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like. This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.',
  },

];

const DefaultLayout = (props: IDefaultLayout) => {
  const { component: Component, ...rest } = props;
  const {Theme: themeStore} = useStores();
  const classes = useStyles(themeStore.internal.theme);
  const [open, setOpen] = useState(true);
  const [searchState, setSearchState] = useState(false);
  const handleDrawerOpen = () => setOpen(true);
  const handleDrawerClose = () => setOpen(false);

  const handleSearchClick = () => {
    setSearchState(!searchState);
    if (!searchState) {
      window.onkeyup = (event) => {
        if (event.code === 'Escape') {
          setSearchState(false);
        }
      }
    } else {
      window.onkeyup = null;
    }
  };
  const LayoutRendered = () => (
        <div className={classes.root}>
          <CssBaseline />
          <AppBar
              color={"primary"}
              position={"absolute"}
              className={clsx(classes.appBar, open && classes.appBarShift)}
          >
            <Collapse
                in={searchState}
                timeout="auto"
                unmountOnExit={true}
            >
              <Paper className={classes.searchRoot} onKeyUp={(event) => {
                if (event.keyCode === 27 /* esc */) {
                  setSearchState(false);
                }}} >
                <IconButton
                    color={"inherit"}
                    onClick={handleDrawerOpen}
                    className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
                >
                  <MenuIcon />
                </IconButton>
                <InputBase
                    autoFocus={true}
                    className={classes.searchInput}
                    placeholder="Search Here!"
                    inputProps={{ 'aria-label': 'search' }}
                />
                <IconButton className={classes.searchIconButton} aria-label="search">
                  <SearchIcon />
                </IconButton>
                <Divider className={classes.searchDivider} orientation="vertical" />
                <IconButton
                    color="primary"
                    className={classes.searchIconButton}
                    aria-label="search"
                    onClick={handleSearchClick}
                >
                  <KeyboardArrowDownIcon />
                </IconButton>
              </Paper>
              <Paper className={classes.searchPaper}>
                {dummyList.map(
                    (item, idx) => {
                      return (
                          <SearchResult {...item} key={idx} />
                      )
                    }
                )}
              </Paper>
            </Collapse>
            <Toolbar className={clsx(classes.toolbar, searchState && classes.toolbarHidden)}>
              <IconButton
                  edge={"start"}
                  color={"inherit"}
                  aria-label={"open drawer"}
                  onClick={handleDrawerOpen}
                  className={clsx(classes.menuButton, open && classes.menuButtonHidden )}
              >
                <MenuIcon/>
              </IconButton>
              <BookIcon className={clsx(classes.logoHeader, open && classes.logoClosed)} />
              <Typography
                  component={'h1'}
                  variant={'h6'}
                  color={"inherit"}
                  noWrap={true}
                  className={classes.title}
              >
                MyLibrary
              </Typography>
              <Tooltip title={"Toogle search"}>
                <IconButton
                    color={"inherit"}
                    onClick={handleSearchClick}
                    className={clsx(classes.searchIcon, !searchState && classes.searchIconClosed)}
                >
                  <SearchIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Check Notifications">
                <IconButton
                    color={"inherit"}
                >
                  <NotificationIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Change theme">
                <IconButton
                    color={"inherit"}
                    onClick={themeStore.changeTheme}
                >
                  <LightBulbIcon />
                </IconButton>
              </Tooltip>
            </Toolbar>
          </AppBar>
          <Drawer
              variant={"permanent"}
              classes={{
                paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose)
              }}
              open={open}
          >
            <div className={classes.toolbarIcon}>
              <BookIcon className={clsx(classes.logo, !open && classes.logoClosed)} />
              <IconButton onClick={handleDrawerClose}>
                <ChevronLeftIcon />
              </IconButton>
            </div>
            <Divider />
            <List
                component="nav"
                aria-labelledby="nested-list-subheader"
            >
              <MenuList />
            </List>
          </Drawer>
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container
                maxWidth={false}
                className={classes.container}
            >
              <Component />
            </Container>
            <footer className={classes.footer}>
              <Typography variant="body2" color="textSecondary" align="center">
                {'Copyright © '}
                <Link color="inherit" href={`${location.host}?love`}>
                  Your Website
                </Link>
                {' '}
                {new Date().getFullYear()}
                {'. Built with '}
                <Link color="inherit" href={`${location.host}?love`}>
                  ♥.
                </Link>
              </Typography>
            </footer>
          </main>
        </div>
  );
  return (
    <Route {...props} component={LayoutRendered} />
  )
};

const WrappedLayout = (props: IDefaultLayout) => {
  useEffect(
    () => {},
    [useStores().Theme.internal.theme]
  );
  return (
    <MuiThemeProvider theme={useStores().Theme.internal.theme}>
      <DefaultLayout {...props} />
    </MuiThemeProvider>
  )
};

export default observer((p: IDefaultLayout) => WrappedLayout(p));
