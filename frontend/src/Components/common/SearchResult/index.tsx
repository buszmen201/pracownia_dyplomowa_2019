import {Card, CardContent, CardHeader, CardMedia, Divider, Typography} from "@material-ui/core";
import {default as React} from "react";
import {useStyles} from "~/Assets/jss/main.jss";
import {useStores} from "~/Stores";

interface ISearchResult {
  callback: () => void;
  image: string;
  title: string;
  author: string;
  desc: string;
}

const SearchResult = (props: ISearchResult) => {
  const {author, title, desc, image, callback} = props;
  const {Theme: themeStores } = useStores();
  const classes = useStyles(themeStores.internal.theme);
  return (
      <>
        <Card className={classes.searchCard} onClick={callback}>
          <CardMedia
              className={classes.searchCover}
              image={image}
              title={`${author} - ${title} cover`}
          />
          <div className={classes.searchDetails}>
            <CardContent className={classes.searchContent}>
              <CardHeader
                  title={title}
                  subheader={author}
              />
              <Typography variant="body2" color="textSecondary" component="div">
                {desc}
              </Typography>
            </CardContent>
          </div>
        </Card>
        <Divider />
      </>
  )
};


export {
  SearchResult,
}
