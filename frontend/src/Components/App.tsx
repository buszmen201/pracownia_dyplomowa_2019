// basics
import {default as React, useEffect, useState } from "react";
// create router
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
// hot reloading -- only dev
import { hot } from "react-hot-loader";
// all routes!
import { routes } from "~Routes";
// all paths will have same layout
import { default as DefaultLayout } from "~Components/common/DefaultLayout";
// this Layout should have themes!
// generic store provider
import {StoresProvider} from "~/Stores";


const LocalRouter = () => {
  const routeMapped = routes.map(
    (route, key) => {
      return (
          <DefaultLayout
              key={key}
              {...route} />
      )
    }
  );
  console.log(routeMapped);
  const wrapWithRouter = (
      <Router><Switch>
        {routeMapped}
      </Switch></Router>
  );
  console.log(wrapWithRouter);
  return wrapWithRouter
};

const App = () => {
  return (
    <StoresProvider>
      <LocalRouter/>
    </StoresProvider>
  );
};

export default hot(module)(App);
