import { default as React, lazy } from 'react';

import { withLazyLoading } from "~Infrastructure/withLazyLoading"

import { default as TrendingPage } from '~Views/Trending'

interface IRoute {
    path: string;
    exact: boolean;
    component: () => JSX.Element;
    name: string;
}

interface IRouter extends Array<IRoute> {
    [index: number]: IRoute;
}

const routes: IRouter = [
    {
      path: '/',
      exact: true,
      component: withLazyLoading(() => <p>test</p>),
      name: 'root-path'
    },
    {
      path: '/trending',
      exact: false,
      component: withLazyLoading(TrendingPage),
      name: 'trending-path',
    }
];

export {
    routes,
    IRoute
};
