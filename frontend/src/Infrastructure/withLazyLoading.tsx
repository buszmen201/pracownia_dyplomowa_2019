import { default as React, Suspense } from 'react';

const withLazyLoading = (Lazy: React.ElementType) => () => (
    <Suspense fallback={<div>Loading...</div>}>
        <Lazy />
    </Suspense>
);

export  {
    withLazyLoading
}
