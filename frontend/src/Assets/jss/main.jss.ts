import { default as React } from 'react'

import {createStyles, makeStyles, useTheme,} from '@material-ui/styles'
import { Theme, colors   } from "@material-ui/core";

const drawerWidth = 240;

const useStyles = makeStyles(
    (theme: Theme) => createStyles({
      root: {
        display: 'flex',

      },
      paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
      },
      fixedHeight: {
        height: 240,
      },
      logo: {
        marginRight: theme.spacing(5),
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        fontSize: theme.typography.h1.fontSize,
        transition: theme.transitions.create('fontSize', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        })
      },
      logoHeader: {
        marginRight: theme.spacing(2),
        fontSize: theme.typography.h4.fontSize,
        transition: theme.transitions.create('fontSize', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        })
      },
      logoHeaderClosed: {
        marginRight: 0,
        fontSize: 0,
        transition: theme.transitions.create('fontSize', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        })
      },
      logoClosed: {
        fontSize: 0,
        transition: theme.transitions.create('fontSize', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen
        })
      },
      searchRoot: {
        padding: '10px 8px',
        display: 'flex',
        borderRadius: 0,
        alignItems: 'center',
            // width: theme.spacing(1),
      },
      searchInput: {
        marginLeft: theme.spacing(1),
        width: '100%',
        flex: 1,
      },
      searchIconButton: {
        padding: 10,
      },
      searchDivider: {
        height: 28,
        margin: 4,
      },
      searchIcon: {
        display: 'none',
      },
      searchIconClosed: {
        display: 'flex',
      },
      searchPaper: {
        maxHeight: '92.8vh',
        backgroundColor: theme.palette.background.paper,
        overflowY: 'scroll'
      },
      searchCard: {
        // backgroundColor: theme.palette.background.paper,
        // color: theme.palette.text.primary,
        // maxWidth: '100%',
        display: 'flex',
      },
      searchDetails: {
        display: 'flex',
        flexDirection: 'column',
        maxWidth: '100%',
        minWidth: '63%',
      },
      searchContent: {
        flex: '1 0 auto',
      },
      searchCover: {
        flexDirection: 'column',
        display: 'flex',
        flex: '0 0 15vw',
        height: '20vw'
      },
     // App bar
      appBar: {
        zIndex: theme.zIndex.drawer + 1,
        // backgroundColor: theme.palette.type === 'light' ? theme.palette.primary.light : theme.palette.primary.dark,
        transition: theme.transitions.create(['width', 'margin'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen
        })
      },
      appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        })
      },

      // App bar -- toolbar
      toolbar: {
        paddingRight: 24,
        display: 'flex'
      },
      toolbarHidden: {
        display: 'none',
      },

      // App bar -- hamburger menu

      menuButton: {
        marginRight: 36,
      },
      menuButtonHidden: {
        display: 'none',
      },

      // App Bar -- title
      title: {
        flexGrow: 1,
      },
      formPaper: {
        padding: theme.spacing(1),
        textAlign: 'center',
        height: '300px',
        color: theme.palette.text.secondary,
      },
      // drawer paper
      drawerPaper: {
        position: "relative",
        whiteSpace: "nowrap",
        width: drawerWidth,
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        })
      },
      drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
          width: theme.spacing(9)
        }
      },

      nested: {
        paddingLeft: theme.spacing(4),
      },

      card: {
        display: 'flex',
        minHeight: theme.spacing(50),
        [theme.breakpoints.down('xs')]: {
          minHeight: 'auto',
        }
      },
      cardDetails: {
        flex: 1,
      },
      cardMedia: {
        width: '281px',
        [theme.breakpoints.down('xs')]: {
          width: 'auto',
        }
      },
      // app bar -- toolbar icon
      toolbarIcon: {
        ...theme.mixins.toolbar,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 0px',
      },

      // content
      appBarSpacer: theme.mixins.toolbar,
      content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
      },
      container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
        flexDirection: 'column',
        minHeight: `calc(100% - 120px)`,

      },
      // footer
      footer: {
        padding: theme.spacing(2),
        marginTop: 'auto',
      },
    }),


);


export {
  useStyles,
}
